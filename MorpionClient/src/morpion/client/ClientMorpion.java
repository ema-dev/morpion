package morpion.client;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ClientMorpion {

    private JFrame frame = new JFrame("Morpion SGC");
    private JLabel messageLabel = new JLabel("");
    private JLabel scoreLabel = new JLabel("Score : 0 ");
    private JTextArea listMessage = new JTextArea(10,200);
    private JTextField chatInput = new JTextField("Entrer un message");

    private static int PORT = 9702;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    
    private Square[] plateau = new Square[9];
    private Square currentSquare;
   
    private int score = 0;

    /*
     * Constructeur ClientMorpion
     */
    public ClientMorpion(String adresseServeur, int score, String listMessageChat) throws Exception {
        this.score = score;
        listMessage.setText(listMessageChat);
        socket = new Socket(adresseServeur, PORT);
        in = new BufferedReader(new InputStreamReader(
            socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        
        
       // Contruction graphique du chat 
        JPanel eastPanel = new JPanel();
        eastPanel.setLayout(new BorderLayout());
        
        
       
        listMessage.setLineWrap(true);
        listMessage.setEditable(false);
        listMessage.setForeground(Color.blue);
        
        JScrollPane chatTextPane = new JScrollPane(listMessage,
           JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
           JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        chatInput.setEnabled(true);
        
        chatInput.addKeyListener(new KeyAdapter() {
        	 public void keyReleased(KeyEvent e) {
        		 int key = e.getKeyCode();
        		 String textMessage = chatInput.getText();
                 if (key == KeyEvent.VK_ENTER && !textMessage.isEmpty()) {  
                    sendChatmessage(textMessage);
                    chatInput.setText("");
                    }
        	      }

			private void sendChatmessage(String text) {
				listMessage.append(listMessage.getText().isEmpty()?text:"\n"+text);
				out.println(Protocole.CHAT_MESSAGE.toString() + " " + text);
			}
		});
        
        eastPanel.add(chatInput, BorderLayout.SOUTH);
        eastPanel.add(chatTextPane, BorderLayout.CENTER);
        eastPanel.setPreferredSize(new Dimension(200, 400));
        eastPanel.setBorder(BorderFactory.createLineBorder(Color.lightGray));
        frame.getContentPane().add(eastPanel, BorderLayout.EAST);
        
        
        //Création de la partie de l'interface graphique donnant des informations au joueur 
        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        
        messageLabel.setBackground(Color.lightGray);
        southPanel.add(messageLabel, BorderLayout.WEST);
        
        scoreLabel.setBackground(Color.lightGray);
        scoreLabel.setText("Score : "+score+" ");
        southPanel.add(scoreLabel, BorderLayout.EAST);
        
        frame.getContentPane().add(southPanel, BorderLayout.SOUTH);
      
        // Création du plateau de jeu 
        JPanel plateauPanel = new JPanel();
        plateauPanel.setBackground(Color.black);
        plateauPanel.setLayout(new GridLayout(3, 3, 2, 2));
        for (int i = 0; i < plateau.length; i++) {
            final int j = i;
            plateau[i] = new Square();
            plateau[i].addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    currentSquare = plateau[j];
                    out.println(Protocole.MOUVEMENT.toString() + " " + j);}});
            
			//System.out.println(Protocole.MOUVEMENT.toString() +" "+ j);
            
            plateauPanel.add(plateau[i]);
        }
        frame.getContentPane().add(plateauPanel, "Center");
    }

    /**
     * La méthode play implémente le protocole de communication avec le serveur
     */
    public Integer play() throws Exception {
        String reponse;
        try {
            reponse = in.readLine();
            if (reponse.startsWith(Protocole.BIENVENUE.toString())) {
                char mark = reponse.charAt(9);
                frame.setTitle("Morpion - Joueur " + mark);
            }
            while (true) {
                reponse = in.readLine();
                if (reponse.startsWith(Protocole.MOUVEMENT_VALIDE.toString())) {
                    messageLabel.setText("Mouvement valide, veuilez patienter");
                    currentSquare.setBackground(Color.red);
                    currentSquare.repaint();
                } else if (reponse.startsWith(Protocole.MOUVEMENT_ADVERSAIRE.toString())) {
                    int loc = Integer.parseInt(reponse.substring(21));
                    plateau[loc].setBackground(Color.BLACK);
                    plateau[loc].repaint();
                    messageLabel.setText("A vous de jouer");
                } else if (reponse.startsWith(Protocole.VICTOIRE.toString())) {
                    messageLabel.setText("Vous avez gagné !");
                    score ++;
                    scoreLabel.setText("Score : "+score);
                    break;
                } else if (reponse.startsWith(Protocole.DEFAITE.toString())) {
                    messageLabel.setText("Vous avez perdu");
                    break;
                } else if (reponse.startsWith(Protocole.EGALITE.toString())) {
                    messageLabel.setText("Egalité");
                    break;
                } else if (reponse.startsWith(Protocole.MESSAGE.toString())) {
                    messageLabel.setText(reponse.substring(8));
                }else if (reponse.startsWith(Protocole.CHAT_MESSAGE.toString())) {
                    listMessage.append(listMessage.getText().isEmpty()?"  >"+reponse.substring(13):"\n" + "   > "+ reponse.substring(13));
                }
            }
            out.println(Protocole.QUITTER.toString());
        }
        finally {
            socket.close();
        }
        return score;
    }

    private boolean wantsToPlayAgain() {
        int response = JOptionPane.showConfirmDialog(frame,
            "Rejouer ?",
            "Morpion",
            JOptionPane.YES_NO_OPTION);
        frame.dispose();
        return response == JOptionPane.YES_OPTION;
    }
 
    private String getListMessageChat (){
    	return listMessage.getText();
    }

    /**
     *Composant graphique de base du tableau de jeu
     */
    @SuppressWarnings("serial")
	static class Square extends JPanel {

        public Square() {
            setBackground(Color.white);
        }

    }

    /**
     * Méthode main lance l'application client
     */
    public static void main(String[] args) throws Exception {
    	int score = 0;
    	String listMessageChat ="";
    	
        while (true) {
            String serverAddress = (args.length == 0) ? "localhost" : args[1];
            ClientMorpion client = new ClientMorpion(serverAddress, score, listMessageChat);
            client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.frame.setSize(480, 320);
            client.frame.setVisible(true);
            client.frame.setResizable(false);
            score = client.play();
            listMessageChat = client.getListMessageChat();
            if (!client.wantsToPlayAgain()) {
                break;
            }
        }
    }
}