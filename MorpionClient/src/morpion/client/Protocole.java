package morpion.client;

public enum Protocole {
BIENVENUE("BIENVENUE"),
QUITTER("QUITTER"),
MOUVEMENT("MOUVEMENT"),
MOUVEMENT_VALIDE("MOUVEMENT_VALIDE"),
MOUVEMENT_ADVERSAIRE("MOUVEMENT_ADVERSAIRE"),
VICTOIRE("VICTOIRE"),
DEFAITE("DEFAITE"),
EGALITE("EGALITE"),
MESSAGE("MESSAGE"),
CHAT_MESSAGE("CHAT_MESSAGE");

private String name = "";

private Protocole(String name) {
	this.name = name;
}

public String toString(){
    return name;
  }
}
